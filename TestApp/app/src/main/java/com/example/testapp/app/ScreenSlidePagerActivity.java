package com.example.testapp.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;


public class ScreenSlidePagerActivity extends FragmentActivity {

    private int numPages;

    private ViewPager mPager;

    private PagerAdapter mPagerAdapter;

    private static final String DEBUG_TAG = "ScreenSlidePagerActivity";

    private Intent data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);

        data = getIntent();

        numPages = data.getIntExtra("size",0);

        int startPosition = data.getIntExtra("position",0);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setCurrentItem(startPosition);
    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Log.i(DEBUG_TAG, "get item " + position);
            ScreenSlidePageFragment screenSlidePageFragment =  new ScreenSlidePageFragment();
            String title = data.getStringExtra("title" + position);
            screenSlidePageFragment.setHeader(title);
            screenSlidePageFragment.setBitmap(MainActivity.getBitmap(title));
            return screenSlidePageFragment;
        }

        @Override
        public int getCount() {
            return numPages;
        }
    }

}