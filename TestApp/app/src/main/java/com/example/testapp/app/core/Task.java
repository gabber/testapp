package com.example.testapp.app.core;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import com.example.testapp.app.MainActivity;
import com.example.testapp.app.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public final class Task extends AsyncTask<Void, String, Boolean> {

    private static final String DOWNLOAD_URL = "http://jsontest111.apiary.io/androids";
    protected final Resources mResources;
    
    private Boolean mResult;
    private String mProgressMessage;
    private IProgressTracker mProgressTracker;

    private MainActivity activity;
    private int errorCode;

    public Task(Resources resources) {

        mResources = resources;

        mProgressMessage = resources.getString(R.string.task_starting);
    }


    public void setProgressTracker(IProgressTracker progressTracker) {

        mProgressTracker = progressTracker;

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
            if (mResult != null) {
            mProgressTracker.onComplete();
            }
        }
    }


    @Override
    protected void onCancelled() {

        mProgressTracker = null;
    }
    

    @Override
    protected void onProgressUpdate(String... values) {

        mProgressMessage = values[0];

        if (mProgressTracker != null) {
            mProgressTracker.onProgress(mProgressMessage);
        }
    }


    @Override
    protected void onPostExecute(Boolean result) {

        mResult = result;

        if (mProgressTracker != null) {
            mProgressTracker.onComplete();
        }

        mProgressTracker = null;
    }


    @Override
    protected Boolean doInBackground(Void... arg0) {

        int progress = 0;
        int progressStep = 0;
        JSONArray jArr = downloadJArray();

        if(jArr!=null){
            if (jArr.length() != 0) {
                progressStep = 100/jArr.length();
            }
            try {
                for(int i=0; i<jArr.length(); i++){
                    if (isCancelled()) {
                        return null;
                    }
                    JSONObject cur = jArr.getJSONObject(i);
                    Bitmap bitmap = null;
                    InputStream iStream = null;
                    try {
                        URL imUrl = new URL(cur.getString("img"));

                        HttpURLConnection urlConnection = (HttpURLConnection) imUrl.openConnection();

                        urlConnection.connect();

                        iStream = urlConnection.getInputStream();
                        bitmap = BitmapFactory.decodeStream(iStream);
                        MainActivity.addBitmap(cur.getString("title"),bitmap);
                    } catch (Exception e) {
                        Log.e("Exception while downloading url", e.toString());
                    } finally {
                        try {
                            iStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    publishProgress(mResources.getString(R.string.task_working,progress+=progressStep));
                }
            }catch (Exception e){
                errorCode = 2;
                e.printStackTrace();
            }
            return true;
        }else{
            return false;
        }

    }

    private JSONArray downloadJArray() {
        try {
            URL urlConn = new URL(DOWNLOAD_URL);

            HttpURLConnection con = (HttpURLConnection) urlConn.openConnection();

            BufferedReader br = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            con.disconnect();
            br.close();
            return new JSONArray(sb.toString());
        }catch (JSONException e){
            errorCode = 1;
        }catch (IOException e){
            errorCode = 2;
        }catch (Exception e){
            errorCode = 3;
        }
        return null;
    }


    public void setActivity(MainActivity activity){
        this.activity = activity;
    }

    public int getErrorCode(){
        return errorCode;
    }
}