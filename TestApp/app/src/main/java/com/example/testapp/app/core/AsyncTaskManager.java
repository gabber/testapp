package com.example.testapp.app.core;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;

import com.example.testapp.app.MainActivity;

public final class AsyncTaskManager implements IProgressTracker, OnCancelListener {
    
    private final OnTaskCompleteListener mTaskCompleteListener;
    private final ProgressDialog mProgressDialog;
    private Task mAsyncTask;
    private MainActivity activity;

    public AsyncTaskManager(MainActivity activity, OnTaskCompleteListener taskCompleteListener) {

        this.activity = activity;
        mTaskCompleteListener = taskCompleteListener;

        mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setCancelable(true);
        mProgressDialog.setOnCancelListener(this);
    }

    public void setupTask(Task asyncTask) {

        mAsyncTask = asyncTask;

        mAsyncTask.setProgressTracker(this);
        mAsyncTask.setActivity(activity);

        mAsyncTask.execute();
    }

    @Override
    public void onProgress(String message) {

        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }

        mProgressDialog.setMessage(message);
    }

    @Override
    public void onCancel(DialogInterface dialog) {

        mAsyncTask.cancel(true);
        mTaskCompleteListener.onTaskComplete(mAsyncTask);
        mAsyncTask = null;
    }
    
    @Override
    public void onComplete() {

        mProgressDialog.dismiss();
        mTaskCompleteListener.onTaskComplete(mAsyncTask);
        mAsyncTask = null;
    }

    public Object retainTask() {

        if (mAsyncTask != null) {
            mAsyncTask.setProgressTracker(null);
        }
        return mAsyncTask;
    }

    public void handleRetainedTask(Object instance) {

        if (instance instanceof Task) {
            mAsyncTask = (Task) instance;
            mAsyncTask.setProgressTracker(this);
        }
    }

    public boolean isWorking() {
        return mAsyncTask != null;
    }
}