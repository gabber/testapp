package com.example.testapp.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.example.testapp.app.core.AsyncTaskManager;
import com.example.testapp.app.core.OnTaskCompleteListener;
import com.example.testapp.app.core.Task;

import java.util.HashMap;


public class MainActivity extends Activity implements OnTaskCompleteListener {

    private AsyncTaskManager mAsyncTaskManager;

    private static HashMap<String,Bitmap> titleBitmap = new HashMap<String, Bitmap>();

    private ListView list;

    private String[] titles;

    private boolean isWorked = false;

    private static final String DEBUG_TAG = "MainActivity";

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent(MainActivity.this, ScreenSlidePagerActivity.class);
        mAsyncTaskManager = new AsyncTaskManager(this, this);

        mAsyncTaskManager.handleRetainedTask(getLastNonConfigurationInstance());


        list = (ListView)findViewById(R.id.listView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i(DEBUG_TAG,"pos="+position+ " "+titles[position]);

                intent.putExtra("size",titleBitmap.size());
                intent.putExtra("curitem",titles[position]);
                intent.putExtra("position",position);

                startActivity(intent);
            }
        });
        if(savedInstanceState!=null){
            isWorked = savedInstanceState.getBoolean("isWorked");
            createListView();
            addTitlesToIntent();
        }

        Log.i(DEBUG_TAG,"onCreate size="+titleBitmap.size());


        Button refresh = (Button)findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(!isWorked) {
                    if (isNetworkAvailable()) {
                        isWorked = true;
                        mAsyncTaskManager.setupTask(new Task(getResources()));
                    }else{
                        showDialog("Ошибка", "Нет соединения с интернетом");
                    }
                }
            }

        });

    }

    @Override
    public Object onRetainNonConfigurationInstance() {

        return mAsyncTaskManager.retainTask();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("isWorked", isWorked);
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
    }



    private boolean isNetworkAvailable(){
        boolean available = false;
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        if(networkInfo !=null && networkInfo.isAvailable())
            available = true;

        return available;
    }

    private void showDialog(String title, String msg){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setMessage(msg)
                .setCancelable(false);

         builder.setNegativeButton("ОК",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }
            );

        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void addBitmap(String name,Bitmap bitmap){
        titleBitmap.put(name, bitmap);
    }


    private void createListView(){
        titles = new String[titleBitmap.size()];
        int i=0;
        for (String key : titleBitmap.keySet() ) {
            titles[i] = key;
            i++;
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, titles);
        list.setAdapter(adapter);

    }

    private void addTitlesToIntent(){
        for(int i=0; i<titles.length; i++){
            intent.putExtra("title"+i,titles[i]);
        }
    }

    @Override
    public void onTaskComplete(Task task) {
        if (task.isCancelled()) {
            showDialog("Ошибка","Вы отменили задачу");

        } else {

            int errorCode = task.getErrorCode();

            if(errorCode!=0){
                switch (errorCode) {
                    case 1:
                        showDialog("Ошибка", "json exseption");
                        break;
                    case 2:
                        showDialog("Ошибка", "IOException");
                        break;
                    case 3:
                        showDialog("Ошибка", "Other exception");
                        break;
                    default:

                }
            }else {
                boolean result;
                try {
                    result = task.get();
                    if (result) {
                        createListView();

                        addTitlesToIntent();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        isWorked = false;
    }


    public static Bitmap getBitmap(String key){
        return titleBitmap.get(key);
    }
}


