package com.example.testapp.app.core;

public interface IProgressTracker {

    void onProgress(String message);

    void onComplete();
}