package com.example.testapp.app.core;

public interface OnTaskCompleteListener {

    void onTaskComplete(Task task);
}